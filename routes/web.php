<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//halaman index
Route::get('/games', 'GameController@index');
// create and save
Route::get('/games/create', 'GameController@create');
Route::post('/games', 'GameController@store');
//halaman show
Route::get('/games/{game_id}', 'GameController@show');
// edit and update
Route::get('/games/{game_id}/edit', 'GameController@edit');
Route::put('/games/{game_id}', 'GameController@update');
//delete
Route::delete('/games/{game_id}', 'GameController@destroy');
