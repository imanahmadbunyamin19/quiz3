<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function index()
    {
        $games = DB::table('games')->get();
        return view('games.index', compact('games'));
    }

    public function create()
    {
        return view('games.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);
        $query = DB::table('games')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);
        return redirect('/games');
    }

    public function show($id)
    {
        $games = DB::table('games')->where('id', $id)->first();
        return view('games.show', compact('games'));
    }
 
    public function edit($id)
    {
        $games = DB::table('games')->where('id', $id)->first();
        return view('games.edit', compact('games'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $query = DB::table('games')
            ->where('id', $id)
            ->update([
                'name' => $request["name"],
                'gameplay' => $request["gameplay"],
                'developer' => $request["developer"],
                'year' => $request["year"]
            ]);
        return redirect('/games');
    }
    
    public function destroy($id)
    {
        $query = DB::table('games')->where('id', $id)->delete();
        return redirect('/games');
    }
}
